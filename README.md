# Ride With Maps

I have made this project for my final exam for Android Developer Course. App shows user current location on Google map and displays latitude, longitude, distance traveled and current speed.

Blog post -> https://amirjanus.gitlab.io/blog/2019/06/03/ride-with-maps/
