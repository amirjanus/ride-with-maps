package com.example.ridewithmaps.maps;

public final class GeoMath {
    
    private static long EARTH_RADIUS = 6371000;
    
    /**
     * Calculates distance between two points based on spherical model of earth.
     *
     * @param latitude0  First point latitude.
     * @param longitude0 First point longitude.
     * @param latitude1  Second point latitude.
     * @param longitude1 Second point longitude.
     * @return Distance in meters.
     */
    public static double ComputeDistance( double latitude0, double longitude0, double latitude1, double longitude1 ) {
        double rLatitude0 = Math.toRadians( latitude0 );
        double rLatitude1 = Math.toRadians( latitude1 );
        
        double dLatitude = Math.toRadians( latitude1 - latitude0 );
        double dLongitude = Math.toRadians( longitude1 - longitude0 );
        
        double a = Math.sin( dLatitude * 0.5 ) * Math.sin( dLatitude * 0.5 ) + Math.cos( rLatitude0 ) * Math.cos( rLatitude1 ) *
                Math.sin( dLongitude * 0.5 ) * Math.sin( dLongitude * 0.5 );
        
        double c = 2 * Math.atan2( Math.sqrt( a ), Math.sqrt( 1 - a ) );
        
        return c * EARTH_RADIUS;
    }
    
    /**
     * Calculates distance between two points based on Pythagoras theorem.
     *
     * @param latitude0  First point latitude.
     * @param longitude0 First point longitude.
     * @param latitude1  Second point latitude.
     * @param longitude1 Second point longitude.
     * @return Distance in kilometers.
     */
    public static double ComputeDistance2( double latitude0, double longitude0, double latitude1, double longitude1 ) {
        double rLatitude0 = Math.toRadians( latitude0 );
        double rLatitude1 = Math.toRadians( latitude1 );
        
        double dLatitude = Math.toRadians( latitude1 - latitude0 );
        double dLongitude = Math.toRadians( longitude1 - longitude0 );
        
        double x = dLongitude * Math.cos( ( rLatitude0 + rLatitude1 ) * 0.5 );
        
        return Math.sqrt( x * x + dLatitude * dLatitude ) * EARTH_RADIUS;
    }
    
    /**
     * Compute initial bearing between two points.
     *
     * @param latitude0  First point latitude.
     * @param longitude0 First point longitude.
     * @param latitude1  Second point latitude.
     * @param longitude1 Second point longitude.
     * @return Bearing on degrees ( 0 - 360 ).
     */
    public static double ComputeBearingInitial( double latitude0, double longitude0, double latitude1, double longitude1 ) {
        double rLatitude0 = Math.toRadians( latitude0 );
        double rLatitude1 = Math.toRadians( latitude1 );
        
        double dLongitude = Math.toRadians( longitude1 - longitude0 );
        
        double x = Math.sin( dLongitude ) * Math.cos( rLatitude1 );
        double y = Math.cos( rLatitude0 ) * Math.sin( rLatitude1 ) - Math.sin( rLatitude0 ) * Math.cos( rLatitude1 ) * Math.cos( dLongitude );
        
        double b = Math.toDegrees( Math.atan2( x, y ) );
        
        return ( b + 360 ) % 360;
    }
    
    /**
     * Compute final bearing between two points.
     *
     * @param latitude0  First point latitude.
     * @param longitude0 First point longitude.
     * @param latitude1  Second point latitude.
     * @param longitude1 Second point longitude.
     * @return Bearing on degrees ( 0 - 360 ).
     */
    public static double ComputeBearingFinal( double latitude0, double longitude0, double latitude1, double longitude1 ) {
        double b = ComputeBearingInitial( latitude1, longitude1, latitude0, longitude0 );
        
        return ( b + 180 ) % 360;
    }
    
    /**
     * Calculates elapsed time between two timestamps.
     *
     * @param timeNanos0 Start time in nanoseconds.
     * @param timeNanos1 End time in nanoseconds.
     * @return Elapsed time in seconds.
     */
    public static double TimeDeltaInSecondsFromNanos( long timeNanos0, long timeNanos1 ) {
        long dTime = timeNanos1 - timeNanos0;
        
        return dTime / 1000000000.0;
    }
    
    /**
     * Calculates speed in meters per second.
     *
     * @param distance Distance in meters.
     * @param time     Time in seconds.
     * @return Speed in meters per second.
     */
    public static double ComputeSpeedInMetersPerSecond( double distance, double time ) {
        return distance / time;
    }
    
    /**
     * Calculates speed in kilometers per hour.
     *
     * @param distance Distance in meters.
     * @param time     Time in seconds.
     * @return Speed in kilometers per hour.
     */
    public static double ComputeSpeedInKilometersPerHour( double distance, double time ) {
        double mps = ComputeSpeedInMetersPerSecond( distance, time );
        
        return mps * 3.6;
    }
    
    /**
     * Calculates speed in kilometers per hour.
     *
     * @param speed Speed in meters per second.
     * @return Speed in kilometers per hour.
     */
    public static double ComputeSpeedInKilometersPerHour( double speed ) {
        return speed * 3.6;
    }
    
    /**
     * Calculates speed in miles per hour.
     *
     * @param distance Distance in meters.
     * @param time     Time in seconds.
     * @return Speed in miles per hour.
     */
    public static double ComputeSpeedInMilesPerHour( double distance, double time ) {
        double mps = ComputeSpeedInMetersPerSecond( distance, time );
        
        return mps * 2.23694;
    }
    
    /**
     * Calculates speed in miles per hour.
     *
     * @param speed Speed in meters per second.
     * @return Speed in miles per hour.
     */
    public static double ComputeSpeedInMilesPerHour( double speed ) {
        return speed * 2.23694;
    }
    
    /**
     * Calculate pace per kilometer.
     *
     * @param time     Time in seconds.
     * @param distance Distance in meters
     * @return Pace per kilometer.
     */
    public static double ComputePacePerKilometer( double time, double distance ) {
        return ( time / 60 ) / ( distance / 1000 );
    }
    
    /**
     * Calculate pace per mile.
     *
     * @param time     Time in seconds.
     * @param distance Distance in meters
     * @return Pace per mile.
     */
    public static double ComputePacePerMile( double time, double distance ) {
        return ( time / 60 ) / ( distance / 1609.34 );
    }
}
