package com.example.ridewithmaps.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

/**
 * Class for managing GPS and location service.
 */
public class LocationTracker {
    
    private static final int REQUEST_CODE = 1;
    
    public LocationManager m_locationManager;
    public LocationListener m_listener;
    private Activity m_activity;
    
    public boolean m_isTracking;
    
    public LocationTracker( Activity activity, LocationListener listener ) {
        m_locationManager = ( LocationManager ) activity.getSystemService( Context.LOCATION_SERVICE );
        m_activity = activity;
        m_listener = listener;
        
        m_isTracking = false;
    }
    
    @SuppressLint( "MissingPermission" )
    public boolean Start() {
        if ( m_isTracking ) {
            return false;
        }
        
        if ( !( CheckLocationPermission() && IsGpsEnabled() ) ) {
            return false;
        }
        
        m_isTracking = true;
        
        m_locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 1000, 1, m_listener );
        
        return true;
    }
    
    public void Stop() {
        if ( !m_isTracking ) {
            return;
        }
        
        m_isTracking = false;
        
        m_locationManager.removeUpdates( m_listener );
    }
    
    public boolean IsGpsEnabled() {
        return m_locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER );
    }
    
    @SuppressLint( "MissingPermission" )
    public Location GetLastKnownLocation() {
        if ( !( CheckLocationPermission() && IsGpsEnabled() ) ) {
            return null;
        }
        
        return m_locationManager.getLastKnownLocation( LocationManager.GPS_PROVIDER );
    }
    
    public void RequestLocationPermission() {
        String[] permissions = new String[] { Manifest.permission.ACCESS_FINE_LOCATION };
        
        ActivityCompat.requestPermissions( m_activity, permissions, 2 );
    }
    
    public boolean CheckLocationPermission() {
        return ActivityCompat.checkSelfPermission( m_activity, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED;
    }
    
    public boolean CheckLocationPermission( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
        if ( requestCode == REQUEST_CODE ) {
            
            for ( int i = 0; i < permissions.length; ++i ) {
                
                if ( permissions[i].equals( Manifest.permission.ACCESS_FINE_LOCATION ) ) {
                    
                    if ( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        return true;
                    }
                    
                    break;
                    
                }
                
            }
            
        }
        
        return false;
    }
}
