package com.example.ridewithmaps.maps;

import android.os.SystemClock;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Holds all data for single ride.
 */
public class RideData {
    
    public static final String SAVE_FOLDER = "RideDataJson";
    
    // Actions are used to compute ride data only.
    public static final short ACTION_START = 0;
    public static final short ACTION_STOP = 1;
    public static final short ACTION_PAUSE = 2;
    public static final short ACTION_RESUME = 3;
    public static final short ACTION_RIDING = 4;
    
    private short m_action;
    
    // States are used to signal is ride running or is paused.
    public static final short STATE_RUNNING = 0;
    public static final short STATE_PAUSED = 1;
    
    private short m_state;
    
    private static final String TAG = "RIDE_ON_RIDE_DATA";
    
    public ArrayList<RideMetrics> m_metrics;
    
    // Standard time and date ( in ms ) since the epoch ( System.currentTimeMillis() ).
    private long m_startTimeStandard;
    private long m_finishTimeStandard;
    
    // Time ( in ms ) since the system was booted ( SystemClock.elapsedRealTime() ).
    private long m_startTimeReal;
    private long m_finishTimeReal;
    
    // Timestamps ( in ms ) from SystemClock.elapsedRealTime() signaling when was ride paused and resumed.
    private long m_pauseTimestamp;
    private long m_resumeTimestamp;
    
    // Cumulative riding and resting times ( in ms ).
    private long m_rideTime;
    private long m_restTime;
    
    private long m_id;
    private String m_filename;
    
    public RideData() {
        m_metrics = new ArrayList<>();
    }
    
    public RideData( JSONObject jsonObject ) {
        m_metrics = new ArrayList<>();
        
        FromJson( jsonObject );
    }
    
    public void AddRideMetricsToList( RideMetrics rideMetrics ) {
        m_metrics.add( rideMetrics );
    }
    
    public long GetRideTime() {
        return m_rideTime;
    }
    
    public void SetRideTime( long rideTime ) {
        m_rideTime += rideTime;
    }
    
    public long GetRestTime() {
        return m_restTime;
    }
    
    public void SetRestTime( long restTime ) {
        m_restTime += restTime;
    }
    
    public void SetPauseTimestamp( long pauseTimestamp ) {
        m_pauseTimestamp = pauseTimestamp;
    }
    
    public long GetPauseTimestamp() {
        return m_pauseTimestamp;
    }
    
    public void SetResumeTimestamp( long resumeTimestamp ) {
        m_resumeTimestamp = resumeTimestamp;
    }
    
    public long GetResumeTimestamp() {
        return m_resumeTimestamp;
    }
    
    /**
     * Creates unique id for comparing RideData classes and filename under which to save this
     * RideData.
     */
    private void GenerateId() {
        m_id = SystemClock.elapsedRealtime();
        m_filename = "user_" + "timestamp_" + m_id;
    }
    
    public long GetId() {
        return m_id;
    }
    
    public void SetAction( short action ) {
        m_action = action;
    }
    
    public short GetAction() {
        return m_action;
    }
    
    public void SetState( short state ) {
        m_state = state;
    }
    
    public short GetState() {
        return m_state;
    }
    
    /**
     * Return last RideMetrics in ride metrics list.
     * @return Last RideMetrics entry in last. Null if list is empty.
     */
    public RideMetrics GetLastMetrics() {
        int size = m_metrics.size();
        
        return size > 0 ? m_metrics.get( size - 1 ) : null;
    }
    
    /**
     * Return filename under which to save file.
     *
     * @return Filename.json
     */
    public String GetFilename() {
        return m_filename + ".json";
    }
    
    /**
     * Set time when this ride started.
     */
    public void SetStartTime( long startTimeStandard, long startTimeReal ) {
        m_startTimeStandard = startTimeStandard;
        m_startTimeReal = startTimeReal;
    }
    
    public long GetStartTimeReal() {
        return m_startTimeReal;
    }
    
    /**
     * Set time when this ride finished.
     */
    public void SetFinishTime( long finishTimeStandard, long finishTimeReal) {
        m_finishTimeStandard = finishTimeStandard;
        m_finishTimeReal = finishTimeReal;
    }
    
    public long GetFinishTimeReal() {
        return m_finishTimeReal;
    }
    
    public void Reset() {
        m_metrics = new ArrayList<>();
        
        m_rideTime = 0;
        m_restTime = 0;
        
        m_startTimeStandard = 0;
        m_finishTimeStandard = 0;
    
        m_startTimeReal = 0;
        m_finishTimeReal = 0;
    
        m_pauseTimestamp = 0;
        m_resumeTimestamp = 0;
        
        m_state = -1;
        m_action = -1;
    }
    
    public ArrayList<RideMetrics> GetMetricsList() {
        return m_metrics;
    }
    
    /**
     * Get latitude and longitude array.
     *
     * @return Array where index i holds latitude and i + 1 holds longitude.
     */
    public double[] GetLatLngArray() {
        int length = m_metrics.size();
    
        double[] latLngArray = new double[0];
    
        if ( length != 0 ) {
        
            latLngArray = new double[length * 2];
        
            for ( int i = 0, j = 0; i < length * 2; i += 2, j += 1 ) {
                latLngArray[i] = m_metrics.get( j ).m_latitude;
                latLngArray[i + 1] = m_metrics.get( j ).m_longitude;
            }
        }
    
        return latLngArray;
    }
    
    /**
     * Save all data from this class to JSON object.
     *
     * @return JSON object.
     */
    public JSONObject ToJson() {
        JSONObject json = new JSONObject();
        
        try {
            
            json.put( "startTime", m_startTimeStandard );
            json.put( "finishTime", m_finishTimeStandard );
            json.put( "rideTime", m_rideTime );
            json.put( "restTime", m_restTime );
            
            JSONArray metricsArray = new JSONArray();
            
            for ( RideMetrics metric : m_metrics ) {
                
                JSONObject jsonMetrics = new JSONObject();
                jsonMetrics.put( "latitude", metric.m_latitude );
                jsonMetrics.put( "longitude", metric.m_longitude );
                jsonMetrics.put( "elapsedTime", metric.m_elapsedTime );
                jsonMetrics.put( "distance", metric.m_distance );
                jsonMetrics.put( "speed", metric.m_speed );
                jsonMetrics.put( "action", metric.m_action );
                
                metricsArray.put( jsonMetrics );
                
            }
            
            json.put( "metrics", metricsArray );
            
            GenerateId();
            
            json.put( "id", m_id );
            
        } catch ( JSONException e ) {
            Log.d( TAG, String.valueOf( e ) );
        }
        
        return json;
    }
    
    public void FromJson( JSONObject jsonObject ) {
        try {
            
            m_startTimeStandard = jsonObject.getLong( "startTime" );
            m_finishTimeStandard = jsonObject.getLong( "finishTime" );
            m_rideTime = jsonObject.getLong( "rideTime" );
            m_restTime = jsonObject.getLong( "restTime" );
            m_id = jsonObject.getLong( "id" );
            
            JSONArray metricsJsonArray = jsonObject.getJSONArray( "metrics" );
            
            for ( int i = 0; i < metricsJsonArray.length(); ++i ) {
                
                JSONObject metricJsonObject = metricsJsonArray.getJSONObject( i );
                
                RideMetrics metric = new RideMetrics();
                
                metric.m_latitude = metricJsonObject.getDouble( "latitude" );
                metric.m_longitude = metricJsonObject.getDouble( "longitude" );
                
                m_metrics.add( metric );
                
            }
            
        } catch ( JSONException e ) {
            Log.d( TAG, String.valueOf( e ) );
        }
    }
    
    /**
     * Get the time when this ride started.
     *
     * @return Time in milliseconds.
     */
    public long GetStartTimeInMillis() {
        return m_startTimeStandard;
    }
    
    /**
     * Get the time when this ride finished.
     *
     * @return Time in milliseconds.
     */
    public long GetFinishTimeInMillis() {
        return m_finishTimeStandard;
    }
    
    
    /**
     * Get ride tine in seconds.
     *
     * @return Ride time in seconds.
     */
    public long GetRideTimeInSeconds() {
        return ( int ) ( m_rideTime / 1000 );
    }
    
    /**
     * Get rest tine in seconds.
     *
     * @return Rest time in seconds.
     */
    public long GetRestTimeInSeconds() {
        return ( int ) ( m_restTime / 1000 );
    }
    
    /**
     * Convert time in milliseconds to date string.
     *
     * @param timeInMillis Time in milliseconds.
     * @return Date string in format DD.MM.YYYY.
     */
    public static String MillisToDateString( long timeInMillis ) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( timeInMillis );
        
        int year = calendar.get( Calendar.YEAR );
        int month = calendar.get( Calendar.MONTH );
        int day = calendar.get( Calendar.DAY_OF_MONTH );
        
        String monthString = month < 10 ? "0" + month : String.valueOf( month );
        String dayString = day < 10 ? "0" + day : String.valueOf( day );
        
        return ( dayString + "." + monthString + "." + year );
    }
    
    /**
     * Convert time in milliseconds to time string.
     *
     * @param timeInMillis Time in milliseconds.
     * @return Time string in format HH:MM:SS.
     */
    public static String MillisToTimeString( long timeInMillis ) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( timeInMillis );
        
        int hour = calendar.get( Calendar.HOUR_OF_DAY );
        int minute = calendar.get( Calendar.MINUTE );
        int second = calendar.get( Calendar.SECOND );
        
        String hourString = hour < 10 ? "0" + hour : String.valueOf( hour );
        String minuteString = minute < 10 ? "0" + minute : String.valueOf( minute );
        String secondString = second < 10 ? "0" + second : String.valueOf( second );
        
        return ( hourString + ":" + minuteString + ":" + secondString );
    }
}
