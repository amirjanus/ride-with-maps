package com.example.ridewithmaps.maps;

import android.os.SystemClock;

public class RideController {
    
    RideData m_rideData;
    
    public RideController( RideData rideData ) {
        m_rideData = rideData;
    }
    
    /**
     * Send location data when new locations fix is posted.
     *
     * @param latitude    Location latitude.
     * @param longitude   Location longitude.
     * @param elapsedTime Location timestamp.
     */
    public void LocationChanged( double latitude, double longitude, long elapsedTime ) {
        // Do not write location to RideData when ride is paused.
        if ( m_rideData.GetState() == RideData.STATE_PAUSED ) {
            ComputeMetricsOnPause();
            
            return;
        }
        
        RideMetrics metrics = new RideMetrics();
        
        metrics.m_latitude = latitude;
        metrics.m_longitude = longitude;
        metrics.m_elapsedTime = elapsedTime;
        
        ComputeMetrics( metrics );
    }
    
    /**
     * Do ride metrics calculations based on current ride state.
     *
     * @param metrics RideMetrics from latest location fix.
     */
    private void ComputeMetrics( RideMetrics metrics ) {
        short currentAction = m_rideData.GetAction();
        
        switch ( currentAction ) {
            case RideData.ACTION_RIDING:
                ComputeMetricsOnRiding( metrics );
                break;
            
            case RideData.ACTION_PAUSE:
                ComputeMetricsOnPause();
                break;
            
            case RideData.ACTION_RESUME:
                ComputeMetricsOnResume( metrics );
                break;
        }
    }
    
    /**
     * Compute ride metrics when ride is in riding state.
     *
     * @param metrics RideMetrics data to compute.
     */
    private void ComputeMetricsOnRiding( RideMetrics metrics ) {
        RideMetrics last = m_rideData.GetLastMetrics();
        
        if ( last == null ) {
            m_rideData.AddRideMetricsToList( metrics );
            
            return;
        }
        
        double dDistance = GeoMath.ComputeDistance( last.m_latitude, last.m_longitude, metrics.m_latitude, metrics.m_longitude );
        metrics.m_distance = last.m_distance + dDistance;
        
        double dTime = GeoMath.TimeDeltaInSecondsFromNanos( last.m_elapsedTime, metrics.m_elapsedTime );
        metrics.m_speed = ( float ) GeoMath.ComputeSpeedInMetersPerSecond( dDistance, dTime );
        
        metrics.m_action = RideData.ACTION_RIDING;
        
        m_rideData.AddRideMetricsToList( metrics );
    }
    
    /**
     * When ride is paused last RideMetrics entry action will be set to paused.
     */
    private void ComputeMetricsOnPause() {
        RideMetrics last = m_rideData.GetLastMetrics();
        
        if ( last == null ) {
            return;
        }
        
        last.m_action = RideData.ACTION_PAUSE;
    }
    
    /**
     * Compute ride metrics when ride is resumed.
     *
     * @param metrics RideMetrics data to compute.
     */
    private void ComputeMetricsOnResume( RideMetrics metrics ) {
        RideMetrics last = m_rideData.GetLastMetrics();
        
        if ( last == null ) {
            m_rideData.AddRideMetricsToList( metrics );
            
            return;
        }
        
        metrics.m_speed = 0;
        metrics.m_distance = last.m_distance;
        metrics.m_action = RideData.ACTION_RESUME;
        
        m_rideData.AddRideMetricsToList( metrics );
        
        // Always switch to riding state after ride was resumed.
        m_rideData.SetAction( RideData.ACTION_RIDING );
    }
    
    /**
     * Compute ride metrics when ride is started.
     *
     * @param metrics RideMetrics data to compute.
     */
    private void ComputeMetricsOnStart( RideMetrics metrics ) {
        metrics.m_distance = 0;
        metrics.m_speed = 0;
        metrics.m_action = RideData.ACTION_START;
        
        // Always switch to riding state after the first location fix was saved.
        m_rideData.SetAction( RideData.ACTION_RIDING );
    }
    
    /**
     * Compute ride metrics when ride is stopped.
     */
    private void ComputeMetricsOnStop() {
        RideMetrics last = m_rideData.GetLastMetrics();
        
        if ( last == null ) {
            return;
        }
        
        last.m_action = RideData.ACTION_STOP;
    }
    
    /**
     * Call this when ride starts.
     */
    public void StartRide() {
        long currentTime = System.currentTimeMillis();
        long realtime = SystemClock.elapsedRealtime();
        
        m_rideData.SetStartTime( currentTime, realtime );
        
        m_rideData.SetAction( RideData.ACTION_RIDING );
        m_rideData.SetResumeTimestamp( realtime );
    }
    
    /**
     * Call this when ride is finished.
     */
    public void FinishRide() {
        long finishTimeStamp = SystemClock.elapsedRealtime();
        
        m_rideData.SetFinishTime( System.currentTimeMillis(), finishTimeStamp );
        
        // Compute rest and ride time.
        short state = m_rideData.GetState();
        
        long timestamp0;
        
        switch ( state ) {
            case RideData.STATE_PAUSED:
                timestamp0 = m_rideData.GetPauseTimestamp();
                m_rideData.SetRestTime( finishTimeStamp - timestamp0 );
                
                break;
            
            case RideData.STATE_RUNNING:
                timestamp0 = m_rideData.GetResumeTimestamp();
                m_rideData.SetRideTime( finishTimeStamp - timestamp0 );
                
                break;
        }
        
        // Set action flag on last metric in RideMetrics list to ACTION_STOP.
        m_rideData.SetAction( RideData.ACTION_STOP );
        
        ComputeMetricsOnStop();
        
        // Save ride data to storage and cloud...
    }
    
    /**
     * Resets ride data.
     */
    public void ResetRide() {
        m_rideData.Reset();
    }
    
    /**
     * Call this when ride is paused.
     */
    public void PauseRide() {
        if ( m_rideData.GetState() == RideData.STATE_RUNNING ) {
            
            // Mark when pause event happened with timestamp.
            long timestamp1 = SystemClock.elapsedRealtime();
            
            m_rideData.SetPauseTimestamp( timestamp1 );
            
            // Calculate ride time from resume timestamp till current timestamp.
            long timestamp0 = m_rideData.GetResumeTimestamp();
            
            m_rideData.SetRideTime( timestamp1 - timestamp0 );
            
            // Set ride to paused state.
            m_rideData.SetState( RideData.STATE_PAUSED );
            
            // Compute ride metrics when ride is paused.
            m_rideData.SetAction( RideData.ACTION_PAUSE );
            ComputeMetricsOnPause();
            
        }
    }
    
    /**
     * Call this after ride is resumed.
     */
    public void ResumeRide() {
        if ( m_rideData.GetState() == RideData.STATE_PAUSED ) {
            
            // Mark when pause event happened with timestamp.
            long timestamp1 = SystemClock.elapsedRealtime();
            
            m_rideData.SetResumeTimestamp( timestamp1 );
            
            // Calculate pause time from resume timestamp till current timestamp.
            long timestamp0 = m_rideData.GetPauseTimestamp();
            
            m_rideData.SetRestTime( timestamp1 - timestamp0 );
            
            // Set ride to resumed state.
            m_rideData.SetState( RideData.STATE_RUNNING );
            
            // Set action flag to ACTION_RESUME; ride metrics will be computed in next location fix.
            m_rideData.SetAction( RideData.ACTION_RESUME );
            
        }
    }
}
