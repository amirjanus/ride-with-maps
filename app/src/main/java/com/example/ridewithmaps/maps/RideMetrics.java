package com.example.ridewithmaps.maps;

/**
 * Holds latitude and longitude and metrics derived from them ( speed, distance ... ).
 */
public class RideMetrics {
    /**
     * Latitude from GPS.
     */
    public double m_latitude;
    /**
     * Longitude from GPS.
     */
    public double m_longitude;
    /**
     * Time in nanoseconds from current location fix.
     */
    public long m_elapsedTime;
    /**
     *  Cumulative distance in meters.
     */
    public double m_distance;
    /**
     * Speed in meters per seconds.
     */
    public float m_speed;
    /**
     * One of ride actions.
     */
    public short m_action;
    
    public RideMetrics() {
        m_latitude = 0;
        m_longitude = 0;
        m_elapsedTime = 0;
        m_distance = 0;
        m_speed = 0;
        m_action = -1;
    }
}
