package com.example.ridewithmaps.maps;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class RideDataViewModel extends ViewModel {
    
    private MutableLiveData<double[]> m_location;
    private RideData m_rideData;
    
    public RideDataViewModel() {
        m_location = new MutableLiveData<>();
        m_rideData = new RideData();
    }
    
    /**
     * Return MutableLiveData so the observer can be added to it.
     *
     * @return MutableLiveData.
     */
    public MutableLiveData<double[]> GetLocation() {
        return m_location;
    }
    
    /**
     * Call this function to set new value to LiveData and dispatched it to active observers.
     *
     * @param latLong Array with latitude at index 0 and longitude at index 1.
     */
    public void SetLocation( double[] latLong ) {
        double[] last = m_location.getValue();
        
        // Do not add new latLong values to LiveData if they are the equal to those already in LiveData
        // to prevent unnecessary calls to maps.
        if ( last != null && ( latLong[0] == last[0] && latLong[1] == last[1] ) ) {
                return;
        }
        
        m_location.setValue( latLong );
    }
    
    /**
     * Get a reference to RideData class.
     *
     * @return RideData reference.
     */
    public RideData GetRideData() {
        return m_rideData;
    }
}
