package com.example.ridewithmaps.maps;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class GoogleMaps implements OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener {
    
    /**
     * Implement this interface to get notified when map is ready to be used.
     */
    public interface MapReadyCallback {
        void OnMapReady();
    }
    
    private MapReadyCallback m_mapReadyCallback;
    
    private static final int m_defaultZoom = 15;
    
    private ImageButton m_btnMyLocation;
    
    public GoogleMap m_map;
    private Marker m_marker;
    private Polyline m_polyline;
    private ArrayList<LatLng> m_polylinePoints;
    
    // Flag to keep track when the user moves camera.
    private boolean m_isCameraMovedByUser;
    // Holds last camera locations before user moved it.
    private double m_cameraLatitude, m_cameraLongitude;
    
    public GoogleMaps() {
        m_map = null;
        m_marker = null;
        m_polyline = null;
        
        m_isCameraMovedByUser = false;
        m_cameraLatitude = 91.0;
        m_cameraLongitude = 181.0;
    }
    
    /**
     * Creates map from dynamic fragment View container in xml layout file.
     *
     * @param activity    Fragment Activity.
     * @param containerId Resource ID of a View container for the map fragment.
     */
    public void CreateFromFragment( FragmentActivity activity, int containerId ) {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        
        FragmentManager manager = activity.getSupportFragmentManager();
        
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add( containerId, mapFragment );
        transaction.commit();
        
        mapFragment.getMapAsync( this );
    }
    
    /**
     * Implement MapReadyCallback interface to get notified when map is ready to be used.
     *
     * @param callback Class that implements MapReadyCallback interface.
     */
    public void SetMapReadyCallback( MapReadyCallback callback ) {
        m_mapReadyCallback = callback;
    }
    
    /**
     * Triggered when the map is ready to be used
     */
    @Override
    public void onMapReady( GoogleMap googleMap ) {
        m_map = googleMap;
        
        if ( m_mapReadyCallback != null ) {
            m_mapReadyCallback.OnMapReady();
        }
        
        m_map.setOnCameraMoveStartedListener( this );
    }
    
    /**
     * Manual implementation of MyLocation button.
     *
     * @param button ImageButton from xml layout.
     */
    public void SetMyLocationButton( ImageButton button ) {
        m_btnMyLocation = button;
        
        m_btnMyLocation.setOnClickListener( view -> {
            HideControls();
            
            MoveCameraToMyLocation();
            
            m_isCameraMovedByUser = false;
        } );
    }
    
    /**
     * Called when the camera starts moving as a result of user action.
     */
    @Override
    public void onCameraMoveStarted( int reason ) {
        switch ( reason ) {
            case GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE:
                ShowControls();
                m_isCameraMovedByUser = true;
                
                break;
            
            case GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION:
                ShowControls();
                m_isCameraMovedByUser = true;
                
                break;
        }
    }
    
    /**
     * Show MyLocation and zoom buttons.
     */
    private void ShowControls() {
        // Prevent unnecessary calls.
        if ( m_isCameraMovedByUser ) {
            return;
        }
        
        UiSettings setting = m_map.getUiSettings();
        setting.setZoomControlsEnabled( true );
        
        if ( m_btnMyLocation != null ) {
            m_btnMyLocation.setVisibility( View.VISIBLE );
        }
    }
    
    /**
     * Hide MyLocation and zoom buttons.
     */
    private void HideControls() {
        UiSettings setting = m_map.getUiSettings();
        setting.setZoomControlsEnabled( false );
        
        if ( m_btnMyLocation != null ) {
            m_btnMyLocation.setVisibility( View.GONE );
        }
    }
    
    /**
     * Creates polyline.
     */
    public void CreatePolyline() {
        CreatePolyline( null );
    }
    
    /**
     * Creates polyline from array with latitude and longitude values.
     *
     * @param latLngArray Array where index i holds latitude and i + 1 holds longitude.
     */
    public void CreatePolyline( double[] latLngArray ) {
        m_polylinePoints = new ArrayList<>();
        
        if ( latLngArray != null && latLngArray.length > 0 ) {
            
            for ( int i = 0; i < latLngArray.length; i += 2 ) {
                m_polylinePoints.add( new LatLng( latLngArray[i], latLngArray[i + 1] ) );
            }
            
        }
        
        PolylineOptions options = new PolylineOptions();
        options.color( 0xFF00910C );
        options.clickable( false );
        
        m_polyline = m_map.addPolyline( options );
        m_polyline.setPoints( m_polylinePoints );
    }
    
    /**
     * Draws polyline with new latitude and longitude points.
     *
     * @param latitude  Latitude.
     * @param longitude Longitude.
     */
    public void UpdatePolyline( double latitude, double longitude ) {
        m_polylinePoints.add( new LatLng( latitude, longitude ) );
        
        m_polyline.setPoints( m_polylinePoints );
    }
    
    /**
     * Removes all of the elements from polyline points list.
     */
    public void ClearPolylinePoints() {
        if ( m_polylinePoints != null ) {
            m_polylinePoints.clear();
            
            if ( m_polyline != null ) {
                m_polyline.setPoints( m_polylinePoints );
            }
        }
    }
    
    /**
     * Creates layer that will hold marker.
     *
     * @param drawableId Marker image resource ID.
     * @param latitude   Latitude at which marker will be drawn.
     * @param longitude  Longitude at which marker will be drawn.
     */
    public void CreateMarker( int drawableId, double latitude, double longitude ) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position( new LatLng( latitude, longitude ) );
        markerOptions.anchor( 0.5f, 0.5f );
        markerOptions.icon( BitmapDescriptorFactory.fromResource( drawableId ) );
        
        m_marker = m_map.addMarker( markerOptions );
        
        m_marker.setDraggable( false );
    }
    
    /**
     * Draws marker at another position.
     *
     * @param latitude  Marker new latitude.
     * @param longitude Marker new longitude.
     */
    public void MoveMarker( double latitude, double longitude ) {
        m_marker.setPosition( new LatLng( latitude, longitude ) );
    }
    
    /**
     * Change camera position.
     *
     * @param latitude  Camera latitude.
     * @param longitude Camera longitude.
     */
    public void MoveCamera( double latitude, double longitude ) {
        m_cameraLatitude = latitude;
        m_cameraLongitude = longitude;
        
        if ( m_isCameraMovedByUser ) {
            return;
        }
        
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom( new LatLng( latitude, longitude ), m_defaultZoom );
        
        m_map.moveCamera( cameraUpdate );
    }
    
    /**
     * Moves camera to the last location provided GPS.
     */
    private void MoveCameraToMyLocation() {
        // Check if camera latitude and longitude are set.
        if ( m_cameraLatitude == 91.0 && m_cameraLongitude == 181.0 ) {
            return;
        }
        
        CameraPosition cameraPosition = new CameraPosition(
                new LatLng( m_cameraLatitude, m_cameraLongitude ),
                m_defaultZoom,
                0,
                0
        );
        
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition( cameraPosition );
        
        m_map.moveCamera( cameraUpdate );
    }
}
