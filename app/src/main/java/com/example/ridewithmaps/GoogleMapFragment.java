package com.example.ridewithmaps;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.ridewithmaps.common.RideStatusReceiver;
import com.example.ridewithmaps.maps.GoogleMaps;
import com.example.ridewithmaps.maps.LocationTracker;
import com.example.ridewithmaps.maps.RideData;
import com.example.ridewithmaps.maps.RideDataViewModel;

public class GoogleMapFragment extends Fragment implements GoogleMaps.MapReadyCallback, RideStatusReceiver.RideStatusCallback {
    
    private ImageButton m_btnMyLocation;
    
    private RideStatusReceiver m_receiver;
    
    private RideDataViewModel m_rideDataViewModel;
    private GoogleMaps m_map;
    private RideData m_rideData;
    
    private boolean m_isStarted;
    
    public GoogleMapFragment() {}
    
    @Override
    public void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
    
        m_isStarted = false;
        
        // Setup ride finished receiver.
        m_receiver = new RideStatusReceiver( this );
        
        IntentFilter filter = new IntentFilter();
        filter.addAction( RideStatusReceiver.BROADCAST_RIDE_STARTED );
        filter.addAction( RideStatusReceiver.BROADCAST_RIDE_FINISHED );
        
        LocalBroadcastManager.getInstance( getContext() ).registerReceiver( m_receiver, filter );
        
        // Setup ride data.
        m_rideDataViewModel = ViewModelProviders.of( getActivity() ).get( RideDataViewModel.class );
        m_rideData = m_rideDataViewModel.GetRideData();
    
        // Setup map.
        m_map = new GoogleMaps();
        m_map.SetMapReadyCallback( this );
    }
    
    @Override
    public View onCreateView( @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_google_map, container, false );
        
        m_map.CreateFromFragment( getActivity(), R.id.frame_google_map );
        
        m_btnMyLocation = view.findViewById( R.id.button_myLocation );
        
        return view;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        
        LocalBroadcastManager.getInstance( getContext() ).unregisterReceiver( m_receiver );
    }
    
    @Override
    public void OnMapReady() {
        LocationTracker locationTracker = new LocationTracker( getActivity(), null );
        Location location = locationTracker.GetLastKnownLocation();
        
        // Set the marker and camera to show last known location.
        if ( location != null ) {
            
            m_map.CreateMarker( R.drawable.marker, location.getLatitude(), location.getLongitude() );
            m_map.MoveCamera( location.getLatitude(), location.getLongitude() );
        
        } else {
            
            // Last know location cant be accessed; create marker somewhere outside map.
            m_map.CreateMarker( R.drawable.marker, 91.0, 181.0 );
            
        }
    
        // Check if some lat/lng points were saved in RideData; if so include them in polyline, otherwise
        // start from scratch.
        double[] latLngArray = m_rideData.GetLatLngArray();
    
        if ( latLngArray.length > 0 ) {
            m_map.CreatePolyline( latLngArray );
        } else {
            m_map.CreatePolyline();
        }
        
        SetupViewModelObservers();
    
        m_map.SetMyLocationButton( m_btnMyLocation );
    }
    
    private void SetupViewModelObservers() {
        m_rideDataViewModel.GetLocation().observe( this, new Observer<double[]>() {
            @Override
            public void onChanged( @Nullable double[] doubles ) {
                if ( doubles != null ) {
                    
                    double latitude = doubles[0];
                    double longitude = doubles[1];
    
                    m_map.MoveMarker( latitude, longitude );
                    m_map.MoveCamera( latitude, longitude );
                    
                    if ( m_isStarted ) {
                        m_map.UpdatePolyline( latitude, longitude );
                    }
                }
            }
        } );
    }
    
    @Override
    public void OnRideStartedCallback() {
        m_isStarted = true;
    }
    
    @Override
    public void OnRideFinishedCallback() {
        m_isStarted = false;
        
        m_map.ClearPolylinePoints();
    }
}
