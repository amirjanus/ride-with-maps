package com.example.ridewithmaps;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ridewithmaps.common.ExitDialog;
import com.example.ridewithmaps.common.RideStatusReceiver;
import com.example.ridewithmaps.maps.GeoMath;
import com.example.ridewithmaps.maps.LocationTracker;
import com.example.ridewithmaps.maps.RideController;
import com.example.ridewithmaps.maps.RideData;
import com.example.ridewithmaps.maps.RideDataViewModel;
import com.example.ridewithmaps.maps.RideMetrics;
import com.example.ridewithmaps.timers.CountingTimer;
import com.example.ridewithmaps.timers.MyTimer;

public class MainActivity extends AppCompatActivity implements LocationListener, MyTimer.OnTimerTickCallback {
    
    private static final String RIDE_STATE = "RIDE_STATE";
    private static final String RIDE_TIME = "RIDE_TIME";
    
    private static final int PERMISSION_REQUEST_CODE = 1;
    
    private LocationTracker m_locationTracker;
    private RideDataViewModel m_rideDataViewModel;
    private RideController m_rideController;
    private RideData m_rideData;
    
    private CountingTimer m_timer;
    private double m_elapsedTime;
    
    private TextView m_textRideTime;
    private TextView m_textRideLatitude;
    private TextView m_textRideLongitude;
    private TextView m_textRideDistance;
    private TextView m_textRideSpeed;
    
    private Button m_btnStart;
    private Button m_btnStop;
    
    private boolean m_isStarted;
    
    private short m_prefSpeed;
    private short m_prefDistance;
    
    // Interface to create function for converting speed.
    interface ConversionSpeed {
        double Convert( double speed );
    }
    
    private ConversionSpeed m_conversionSpeed;
    private int m_stringSpeedId;
    
    // Interface to create function for converting distance.
    interface ConversionDistance {
        double Convert( double distance );
    }
    
    private ConversionDistance m_conversionDistance;
    private int m_stringDistanceId;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        
        setContentView( R.layout.activity_main );
        
        // Setup shared preferences.
        PreferenceManager.setDefaultValues( this, R.xml.preferences, false );
        
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences( this );
        m_prefSpeed = Short.parseShort( sharedPreferences.getString( SettingsActivity.PREF_KEY_SPEED, "1" ) );
        m_prefDistance = Short.parseShort( sharedPreferences.getString( SettingsActivity.PREF_KEY_DISTANCE, "1" ) );
        
        SetupConversionLambdas();
        
        m_isStarted = false;
        
        // Setup location service.
        m_locationTracker = new LocationTracker( this, this );
        m_locationTracker.Start();
        
        // Setup activity data.
        m_rideDataViewModel = ViewModelProviders.of( this ).get( RideDataViewModel.class );
        m_rideData = m_rideDataViewModel.GetRideData();
        m_rideController = new RideController( m_rideData );
        
        // Setup timer.
        m_timer = new CountingTimer();
        m_timer.SetTickCallback( this );
        m_elapsedTime = 0;
        
        RequestPermissions();
        
        InitViews();
        SetupButtonListeners();
        
        ShowOrHideButton();
        ResetDashboardText();
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
        if ( m_isStarted ) {
            
            m_rideController.ResumeRide();
            
            m_timer.Start( m_elapsedTime );
            
        }
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        
        if ( m_isStarted ) {
            
            m_rideController.PauseRide();
            
            m_timer.Stop();
            
            m_elapsedTime = m_timer.GetElapsedTime();
            
        }
    }
    
    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        outState.putBoolean( RIDE_STATE, m_isStarted );
        outState.putDouble( RIDE_TIME, m_elapsedTime );
        
        super.onSaveInstanceState( outState );
    }
    
    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState ) {
        super.onRestoreInstanceState( savedInstanceState );
        
        m_isStarted = savedInstanceState.getBoolean( RIDE_STATE );
        m_elapsedTime = savedInstanceState.getDouble( RIDE_TIME );
        
        if ( m_isStarted ) {
            
            ShowOrHideButton();
            
            m_rideController.ResumeRide();
            
            m_timer.Start( m_elapsedTime );
            
            Intent startedIntent = new Intent( RideStatusReceiver.BROADCAST_RIDE_STARTED );
            LocalBroadcastManager.getInstance( this ).sendBroadcast( startedIntent );
            
        }
    }
    
    /**
     * Call to start ride activity.
     */
    private void Start() {
        if ( m_isStarted ) {
            return;
        }
        
        m_isStarted = true;
        
        m_rideController.StartRide();
        
        m_timer.Start();
        
        ShowOrHideButton();
        
        Intent startedIntent = new Intent( RideStatusReceiver.BROADCAST_RIDE_STARTED );
        LocalBroadcastManager.getInstance( this ).sendBroadcast( startedIntent );
    }
    
    /**
     * Call to stop ride activity.
     */
    private void Stop() {
        if ( !m_isStarted ) {
            return;
        }
        
        m_isStarted = false;
        
        m_rideController.FinishRide();
        m_rideController.ResetRide();
        
        m_timer.Stop();
        
        ShowOrHideButton();
        ResetDashboardText();
        
        Intent finishedIntent = new Intent( RideStatusReceiver.BROADCAST_RIDE_FINISHED );
        LocalBroadcastManager.getInstance( this ).sendBroadcast( finishedIntent );
    }
    
    @Override
    public void OnTickCallback() {
        if ( m_isStarted ) {
            UpdateDashboardText();
        }
    }
    
    @Override
    public void onLocationChanged( Location location ) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        long elapsedTime = location.getElapsedRealtimeNanos();
        
        double[] latLng = { latitude, longitude };
        
        m_rideDataViewModel.SetLocation( latLng );
        
        m_rideController.LocationChanged( latitude, longitude, elapsedTime );
    }
    
    @Override
    public void onStatusChanged( String s, int i, Bundle bundle ) {
    }
    
    @Override
    public void onProviderEnabled( String s ) {
    }
    
    @Override
    public void onProviderDisabled( String s ) {
    }
    
    private void RequestPermissions() {
        if ( m_locationTracker.CheckLocationPermission() ) {
            return;
        }
        
        String[] permissions = new String[] {
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        
        ActivityCompat.requestPermissions( this, permissions, PERMISSION_REQUEST_CODE );
    }
    
    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
        // Start location tracking again because it wont work without location permission.
        if ( requestCode == PERMISSION_REQUEST_CODE ) {
            
            if ( permissions[0].equals( Manifest.permission.ACCESS_FINE_LOCATION ) && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                
                m_locationTracker.Start();
                
            }
            
        }
    }
    
    /**
     * Creates functions to convert speed and distance units based on user preferences.
     */
    private void SetupConversionLambdas() {
        // Lambdas for converting speed.
        ConversionSpeed speedMps = ( speed ) -> speed;
        ConversionSpeed speedKph = ( speed ) -> GeoMath.ComputeSpeedInKilometersPerHour( speed );
        ConversionSpeed speedMph = ( speed ) -> GeoMath.ComputeSpeedInMilesPerHour( speed );
        
        switch ( m_prefSpeed ) {
            case 0:
                m_conversionSpeed = speedMps;
                m_stringSpeedId = R.string.text_ride_speed_mps;
                break;
            
            case 1:
                m_conversionSpeed = speedKph;
                m_stringSpeedId = R.string.text_ride_speed_kmh;
                break;
            
            case 2:
                m_conversionSpeed = speedMph;
                m_stringSpeedId = R.string.text_ride_speed_mph;
                break;
            
            default:
                m_conversionSpeed = null;
                m_stringSpeedId = R.string.text_ride_speed_mps;
        }
    
        // Lambdas for converting distance.
        ConversionDistance distanceMeters = ( distance ) -> distance;
        ConversionDistance distanceKilometers = ( distance ) -> distance * 0.001;
        ConversionDistance distanceMiles = ( distance ) -> distance * 0.000621371192;
        
        switch ( m_prefDistance ) {
            case 0:
                m_conversionDistance = distanceMeters;
                m_stringDistanceId = R.string.text_ride_distance_meters;
                break;
            
            case 1:
                m_conversionDistance = distanceKilometers;
                m_stringDistanceId = R.string.text_ride_distance_km;
                break;
            
            case 2:
                m_conversionDistance = distanceMiles;
                m_stringDistanceId = R.string.text_ride_distance_miles;
                break;
            
            default:
                m_conversionDistance = null;
                m_stringDistanceId = R.string.text_ride_distance_meters;
        }
    }
    
    /**
     * Shows or hides start and stop buttons.
     */
    private void ShowOrHideButton() {
        if ( m_isStarted ) {
            
            m_btnStart.setVisibility( View.GONE );
            m_btnStop.setVisibility( View.VISIBLE );
            
        } else {
            
            m_btnStart.setVisibility( View.VISIBLE );
            m_btnStop.setVisibility( View.GONE );
            
        }
    }
    
    /**
     * Updates TextViews that show time, location, speed and distance.
     */
    private void UpdateDashboardText() {
        long elapsedTime = ( long ) m_timer.GetElapsedTime();
        
        m_textRideTime.setText( getString( R.string.text_ride_time, CountingTimer.GetHourMinuteSecond( elapsedTime ) ) );
        
        RideMetrics rideMetrics = m_rideData.GetLastMetrics();
        
        if ( rideMetrics != null ) {
            
            m_textRideLatitude.setText( getString( R.string.text_ride_latitude, rideMetrics.m_latitude ) );
            m_textRideLongitude.setText( getString( R.string.text_ride_longitude, rideMetrics.m_longitude ) );
            
            double speed = m_conversionSpeed.Convert( rideMetrics.m_speed );
            m_textRideSpeed.setText( getString( m_stringSpeedId, speed ) );
            
            double distance = m_conversionDistance.Convert( rideMetrics.m_distance );
            m_textRideDistance.setText( getString( m_stringDistanceId, distance ) );
            
        }
    }
    
    /**
     * Set all TextView elements in dashboard to zero.
     */
    private void ResetDashboardText() {
        m_textRideTime.setText( getString( R.string.text_ride_time, "00:00:00" ) );
        m_textRideLatitude.setText( getString( R.string.text_ride_latitude, 0.0 ) );
        m_textRideLongitude.setText( getString( R.string.text_ride_longitude, 0.0 ) );
        m_textRideSpeed.setText( getString( m_stringSpeedId, 0.0 ) );
        m_textRideDistance.setText( getString( m_stringDistanceId, 0.0 ) );
    }
    
    private void InitViews() {
        // Buttons.
        m_btnStart = findViewById( R.id.button_start );
        m_btnStop = findViewById( R.id.button_stop );
        
        // TextViews.
        m_textRideTime = findViewById( R.id.text_rideTime );
        m_textRideLatitude = findViewById( R.id.text_rideLatitude );
        m_textRideLongitude = findViewById( R.id.text_rideLongitude );
        m_textRideDistance = findViewById( R.id.text_rideDistance );
        m_textRideSpeed = findViewById( R.id.text_rideSpeed );
    }
    
    private void SetupButtonListeners() {
        // Buttons.
        m_btnStart.setOnClickListener( view -> Start() );
        
        m_btnStop.setOnClickListener( view -> Stop() );
    }
    
    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_main, menu );
        
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch ( item.getItemId() ) {
            case R.id.menu_settings:
                Intent intent = new Intent( this, SettingsActivity.class );
                startActivity( intent );
                
                return true;
            
            case R.id.menu_exit:
                ExitDialog exitDialog = new ExitDialog();
                exitDialog.show( getSupportFragmentManager(), "Exit Dialog" );
                
                return true;
            
            default:
                return super.onOptionsItemSelected( item );
        }
    }
}