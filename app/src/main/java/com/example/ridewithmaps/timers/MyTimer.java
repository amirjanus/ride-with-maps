package com.example.ridewithmaps.timers;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.util.Timer;
import java.util.TimerTask;

public abstract class MyTimer {
    
    public interface OnTimerTickCallback {
        void OnTickCallback();
    }
    
    private OnTimerTickCallback m_tickCallback;
    
    protected double m_elapsedTime;
    private double m_lastTime;
    
    protected boolean m_isStarted;
    protected boolean m_isPaused;
    
    private Timer m_timer;
    private TimerTask m_timerTask;
    
    MyTimer() {
    }
    
    public void SetTickCallback( OnTimerTickCallback callback ) {
        m_tickCallback = callback;
    }
    
    /**
     * Create Timer classes and timer loop.
     */
    private void SetupTimer() {
        m_timer = new Timer();
        
        // Handler to communicate with UI thread.
        final Handler handler = new Handler( Looper.getMainLooper() );
        
        // OnTimerTickCallback.OnTickCallback function will run on UI thread.
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                m_tickCallback.OnTickCallback();
            }
        };
        
        // Timer loop.
        m_timerTask = new TimerTask() {
            @Override
            public void run() {
                if ( !m_isPaused ) {
                    
                    double timestamp = SystemClock.elapsedRealtime() / 1000.0;
                    
                    m_elapsedTime += timestamp - m_lastTime;
                    
                    m_lastTime = timestamp;
                    
                    Tick();
                    
                    handler.post( runnable );
                    
                }
            }
        };
    }
    
    /**
     * Function to execute every timer tick.
     */
    protected abstract void Tick();
    
    /**
     * Start timer.
     */
    public void Start() {
        if ( m_isStarted ) {
            return;
        }
        
        Reset();
        
        m_isStarted = true;
        
        SetupTimer();
        
        m_lastTime = SystemClock.elapsedRealtime() / 1000.0;
        
        m_timer.scheduleAtFixedRate( m_timerTask, 0, 100L );
    }
    
    /**
     * Start timer with predefined elapsed time.
     *
     * @param elapsedTime Time in seconds.
     */
    public void Start( double elapsedTime ) {
        if ( m_isStarted ) {
            return;
        }
        
        Reset();
        
        m_isStarted = true;
        
        m_elapsedTime = elapsedTime;
        
        SetupTimer();
        
        m_lastTime = SystemClock.elapsedRealtime() / 1000.0;
        
        m_timer.scheduleAtFixedRate( m_timerTask, 0, 100L );
    }
    
    /**
     * Stop timer ( and its thread ).
     */
    public void Stop() {
        if ( !m_isStarted ) {
            return;
        }
        
        m_timer.cancel();
        
        m_isStarted = false;
        
        double stopTimestamp = SystemClock.elapsedRealtime() / 1000.0;
        
        m_elapsedTime += stopTimestamp - m_lastTime;
    }
    
    /**
     * Resume timer after it was paused.
     */
    public void Resume() {
        if ( !m_isStarted ) {
            return;
        }
        
        m_lastTime = SystemClock.elapsedRealtime() / 1000.0;
        
        m_isPaused = false;
    }
    
    /**
     * Pause timer. This only stops timer from counting elapsed time; it does not delete timer.
     */
    public void Pause() {
        if ( !m_isStarted ) {
            return;
        }
        
        m_isPaused = true;
        
        double pauseTimestamp = SystemClock.elapsedRealtime() / 1000.0;
        
        m_elapsedTime += pauseTimestamp - m_lastTime;
    }
    
    /**
     * Resets all timer variables to beginning.
     */
    protected void Reset() {
        m_isStarted = false;
        m_isPaused = false;
        
        m_elapsedTime = 0;
        m_lastTime = 0;
    }
    
    /**
     * Return time in hours, minutes and seconds.
     *
     * @param seconds Time in seconds.
     * @return Elapsed time in format HH:MM:SS.
     */
    public static String GetHourMinuteSecond( long seconds ) {
        int h = ( int ) ( seconds / 3600 );
        int m = ( int ) ( seconds / 60 % 60 );
        int s = ( int ) ( seconds % 60 );
        
        String hs = h < 10 ? ( "0" + h ) : Integer.toString( h );
        String ms = m < 10 ? ( "0" + m ) : Integer.toString( m );
        String ss = s < 10 ? ( "0" + s ) : Integer.toString( s );
        
        return hs + ":" + ms + ":" + ss;
    }
    
    /**
     * Check if timer is running.
     */
    public boolean GetIsStarted() {
        return m_isStarted;
    }
    
    /**
     * Check if timer is paused.
     */
    public boolean GetIsPaused() {
        return m_isPaused;
    }
    
    /**
     * Returns elapsed time.
     *
     * @return Elapsed time in seconds.
     */
    public double GetElapsedTime() {
        return m_elapsedTime;
    }
}
