package com.example.ridewithmaps.timers;

public class CountdownTimer extends MyTimer {
    
    public interface OnTimerStopCallback {
        void OnStopCallback();
    }
    
    private OnTimerStopCallback m_stopCallback;
    
    // Time time this Timer will run.
    private double m_countdownTime;
    // Time left before Timer reaches zero.
    private double m_remainingTime;
    
    public CountdownTimer( long countdownTime ) {
        super();
        
        m_countdownTime = countdownTime;
        m_remainingTime = countdownTime;
    }
    
    public void SetStopCallback( OnTimerStopCallback callback ) {
        m_stopCallback = callback;
    }
    
    @Override
    protected void Tick() {
        m_remainingTime = m_countdownTime - m_elapsedTime;
        
        if ( m_remainingTime >= 0.05 ) {
            
            m_stopCallback.OnStopCallback();
            
            Stop();
            
        }
    }
    
    @Override
    protected void Reset() {
        m_remainingTime = 0;
        
        super.Reset();
    }
    
    /**
     * Set number of seconds the timer will countdown.
     */
    public void SetCountdownTime( double seconds ) {
        m_countdownTime = seconds;
    }
    
    /**
     * Return number of seconds before end.
     */
    public double GetRemainingTime() {
        return m_remainingTime;
    }
    
    /**
     * Increase countdown time to postpone end.
     *
     * @param time Seconds to add to countdown.
     */
    public void AddMoreTime( double time ) {
        m_countdownTime += time;
        
        m_remainingTime = m_countdownTime - m_elapsedTime;
    }
}
