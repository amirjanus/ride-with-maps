package com.example.ridewithmaps;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {
    
    public static final String PREF_KEY_SPEED = "preferences_speed";
    public static final String PREF_KEY_DISTANCE = "preferences_distance";
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        
        getSupportFragmentManager().beginTransaction()
                .replace( android.R.id.content, new SettingsFragment() )
                .commit();
    }
}
