package com.example.ridewithmaps.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Use this class to send broadcast when the user started and finished activity.
 */
public class RideStatusReceiver extends BroadcastReceiver {
    
    public interface RideStatusCallback {
        void OnRideFinishedCallback();
        void OnRideStartedCallback();
    }
    
    public static final String BROADCAST_RIDE_STARTED = "BROADCAST_RIDE_STARTED";
    public static final String BROADCAST_RIDE_FINISHED = "BROADCAST_RIDE_FINISHED";
    
    private RideStatusCallback m_callback;
    
    public RideStatusReceiver( RideStatusCallback callback ) {
        m_callback = callback;
    }
    
    @Override
    public void onReceive( Context context, Intent intent ) {
        String intentAction = intent.getAction();
        
        if ( intentAction != null ) {
            
            switch ( intentAction ) {
                case BROADCAST_RIDE_STARTED:
                    m_callback.OnRideStartedCallback();
                    break;
                    
                case BROADCAST_RIDE_FINISHED:
                    m_callback.OnRideFinishedCallback();
                    break;
            }
            
        }
    }
}
